/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2015  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GUIHANDLER_H
#define GUIHANDLER_H

#include <string>

/**
 * This class is an interface for browser engines to interact with GUIs
 **/
class GUIHandler
{
  public:
    /**
     * DisplayCurrentTitle is called if the browser engine requests the page title to be displayed to the user
     **/
    virtual void DisplayCurrentTitle(std::string title) = 0;
    
    /**
     * DisplayCurrentURI is called if the engine requests the current URI to be displayed to the user
     **/
    virtual void DisplayCurrentURI(std::string uri) = 0;
};

#endif