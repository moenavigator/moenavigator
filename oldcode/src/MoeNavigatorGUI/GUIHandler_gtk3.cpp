/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2015  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GUIHandler_gtk3.h"

void GUIHandler_gtk3::SetMainGUIElement(Glib::RefPtr<Gtk::Builder> mainGUIElement)
{
  this->mainGUIElement = mainGUIElement;
}


void GUIHandler_gtk3::DisplayCurrentTitle(std::string title)
{
  if(this->mainGUIElement != NULL)
  {
    //check for window title (later: tab title) and set it
    Gtk::Window* mainWindow;
    this->mainGUIElement->get_widget("MoeNavigatorMain", mainWindow);
    if(mainWindow != NULL)
    {
      //window found: set title
      mainWindow->set_title(title);
    }
  }
}


void GUIHandler_gtk3::DisplayCurrentURI(std::string uri)
{
  if(this->mainGUIElement != NULL)
  {
    //check for uri entry field and set it to the current uri:
    Gtk::Entry* uriEntry;
    this->mainGUIElement->get_widget("URLEntry", uriEntry);
    if(uriEntry != NULL)
    {
      //entry field found: set uri
      uriEntry->set_text(uri);
    }
  }
}
