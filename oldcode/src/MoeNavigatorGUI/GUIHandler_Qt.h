/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GUIHANDLER_QT_H
#define GUIHANDLER_QT_H

#include <QtWidgets>
#include <QUiLoader>
#include <QObject>


#include <string>
#include "GUIHandler.h"

#include "../EngineInterfaces/MoeNavigatorEngineInterface.h"




class GUIHandler_Qt: public GUIHandler, public QObject
{
  public:
    GUIHandler_Qt();
    EngineInterface* GetEnginePointer();
    
    
  public Q_SLOT:
    void DisplayCurrentTitle(std::string title);
    void DisplayCurrentURI(std::string uri);
    
  private:
    EngineInterface* engine;
    QWidget* mainWindow;
    std::vector<QWidget*>* titleElements; //list of GUI elements that display the web page's title
    std::vector<QWidget*>* uriElements; //list of GUI elements that display the web page's URI
};

#endif
