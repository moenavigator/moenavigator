/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GUIHandler_Qt.h"

#include "../EngineInterfaces/MoeNavigatorEngineInterface.h"




GUIHandler_Qt::GUIHandler_Qt()
{
  //Engine initialisation:
  this->engine = new MoeNavigatorEngineInterface();

  //GUI element list initialisation:
  this->titleElements = new std::vector<QWidget*>();
  this->uriElements = new std::vector<QWidget*>();
  
  //GUI initialisation:
  QUiLoader loader;
  QFile uiFile("./src/MainGUI_QT.ui");
  uiFile.open(QFile::ReadOnly);
  
  this->mainWindow = loader.load(&uiFile);
  uiFile.close();
  
  QWidget* engineContainer = this->mainWindow->findChild<QWidget*>("engineContainer");
  QWidget* addressBar = this->mainWindow->findChild<QWidget*>("addressBar");
  this->uriElements->push_back(addressBar);
  
  this->engine->SetParentWidget(engineContainer);
  
  //connect widget signals: FIXME
  QObject::connect(addressBar, SIGNAL(returnPressed()),this->engine,SLOT(EngineInterface::OpenURL));
  
  
  //show main window:
  if(this->mainWindow != NULL)
  {
    this->mainWindow->show();
  }
}

EngineInterface* GUIHandler_Qt::GetEnginePointer()
{
  return this->engine;
}


void GUIHandler_Qt::DisplayCurrentTitle(std::string title)
{
  
}


void GUIHandler_Qt::DisplayCurrentURI(std::string uri)
{
  
}
