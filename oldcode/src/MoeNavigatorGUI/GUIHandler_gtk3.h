/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2015  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GUIHANDLER_GTK3_H
#define GUIHANDLER_GTK3_H

#include "GUIHandler.h"
#include <gtkmm.h>


class GUIHandler_gtk3: public GUIHandler
{
  public:
    void SetMainGUIElement(Glib::RefPtr<Gtk::Builder> mainGUIElement);
    void DisplayCurrentTitle(std::string title);
    void DisplayCurrentURI(std::string uri);
  
  private:
    Glib::RefPtr<Gtk::Builder> mainGUIElement;
};

#endif