/********************************************************************************
** Form generated from reading UI file 'MainGUI_QT.ui'
**
** Created: Wed Mar 4 22:34:54 2015
**      by: Qt User Interface Compiler version 4.8.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef MOENAVIGATORGUI_QT_H
#define MOENAVIGATORGUI_QT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MoeNavigatorWindow
{
public:
    QAction *actionOpen;
    QAction *actionQuit;
    QAction *actionNext;
    QAction *actionBackward;
    QAction *actionAllow_Images;
    QAction *actionAllow_JavaScript;
    QAction *actionAllow_Audio;
    QAction *actionAllow_Video;
    QAction *actionSettings;
    QAction *actionExtensions;
    QAction *actionCut;
    QAction *actionCopy;
    QAction *actionPaste;
    QWidget *centralwidget;
    QWidget *mainBar;
    QPushButton *prevButton;
    QPushButton *nextButton;
    QLineEdit *addressBar;
    QWidget *engineContainer;
    QWidget *pageBar;
    QPushButton *imagesButton;
    QPushButton *imagesButton_2;
    QPushButton *imagesButton_3;
    QPushButton *imagesButton_4;
    QPushButton *imagesButton_5;
    QMenuBar *menubar;
    QMenu *menuNavigate;
    QMenu *menuEdit;
    QMenu *menuView;
    QMenu *menuPage;
    QMenu *menuOptions;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MoeNavigatorWindow)
    {
        if (MoeNavigatorWindow->objectName().isEmpty())
            MoeNavigatorWindow->setObjectName(QString::fromUtf8("MoeNavigatorWindow"));
        MoeNavigatorWindow->resize(640, 480);
        actionOpen = new QAction(MoeNavigatorWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        actionQuit = new QAction(MoeNavigatorWindow);
        actionQuit->setObjectName(QString::fromUtf8("actionQuit"));
        actionNext = new QAction(MoeNavigatorWindow);
        actionNext->setObjectName(QString::fromUtf8("actionNext"));
        actionBackward = new QAction(MoeNavigatorWindow);
        actionBackward->setObjectName(QString::fromUtf8("actionBackward"));
        actionAllow_Images = new QAction(MoeNavigatorWindow);
        actionAllow_Images->setObjectName(QString::fromUtf8("actionAllow_Images"));
        actionAllow_JavaScript = new QAction(MoeNavigatorWindow);
        actionAllow_JavaScript->setObjectName(QString::fromUtf8("actionAllow_JavaScript"));
        actionAllow_Audio = new QAction(MoeNavigatorWindow);
        actionAllow_Audio->setObjectName(QString::fromUtf8("actionAllow_Audio"));
        actionAllow_Video = new QAction(MoeNavigatorWindow);
        actionAllow_Video->setObjectName(QString::fromUtf8("actionAllow_Video"));
        actionSettings = new QAction(MoeNavigatorWindow);
        actionSettings->setObjectName(QString::fromUtf8("actionSettings"));
        actionExtensions = new QAction(MoeNavigatorWindow);
        actionExtensions->setObjectName(QString::fromUtf8("actionExtensions"));
        actionCut = new QAction(MoeNavigatorWindow);
        actionCut->setObjectName(QString::fromUtf8("actionCut"));
        actionCopy = new QAction(MoeNavigatorWindow);
        actionCopy->setObjectName(QString::fromUtf8("actionCopy"));
        actionPaste = new QAction(MoeNavigatorWindow);
        actionPaste->setObjectName(QString::fromUtf8("actionPaste"));
        centralwidget = new QWidget(MoeNavigatorWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        mainBar = new QWidget(centralwidget);
        mainBar->setObjectName(QString::fromUtf8("mainBar"));
        mainBar->setGeometry(QRect(0, 0, 641, 31));
        prevButton = new QPushButton(mainBar);
        prevButton->setObjectName(QString::fromUtf8("prevButton"));
        prevButton->setGeometry(QRect(0, 0, 31, 31));
        nextButton = new QPushButton(mainBar);
        nextButton->setObjectName(QString::fromUtf8("nextButton"));
        nextButton->setGeometry(QRect(30, 0, 31, 31));
        addressBar = new QLineEdit(mainBar);
        addressBar->setObjectName(QString::fromUtf8("addressBar"));
        addressBar->setGeometry(QRect(60, 0, 581, 31));
        engineContainer = new QWidget(centralwidget);
        engineContainer->setObjectName(QString::fromUtf8("engineContainer"));
        engineContainer->setGeometry(QRect(0, 30, 641, 381));
        pageBar = new QWidget(centralwidget);
        pageBar->setObjectName(QString::fromUtf8("pageBar"));
        pageBar->setGeometry(QRect(0, 410, 641, 21));
        imagesButton = new QPushButton(pageBar);
        imagesButton->setObjectName(QString::fromUtf8("imagesButton"));
        imagesButton->setGeometry(QRect(0, 0, 91, 21));
        imagesButton_2 = new QPushButton(pageBar);
        imagesButton_2->setObjectName(QString::fromUtf8("imagesButton_2"));
        imagesButton_2->setGeometry(QRect(90, 0, 91, 21));
        imagesButton_3 = new QPushButton(pageBar);
        imagesButton_3->setObjectName(QString::fromUtf8("imagesButton_3"));
        imagesButton_3->setGeometry(QRect(180, 0, 91, 21));
        imagesButton_4 = new QPushButton(pageBar);
        imagesButton_4->setObjectName(QString::fromUtf8("imagesButton_4"));
        imagesButton_4->setGeometry(QRect(270, 0, 91, 21));
        imagesButton_5 = new QPushButton(pageBar);
        imagesButton_5->setObjectName(QString::fromUtf8("imagesButton_5"));
        imagesButton_5->setGeometry(QRect(360, 0, 91, 21));
        MoeNavigatorWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MoeNavigatorWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 640, 25));
        menuNavigate = new QMenu(menubar);
        menuNavigate->setObjectName(QString::fromUtf8("menuNavigate"));
        menuEdit = new QMenu(menubar);
        menuEdit->setObjectName(QString::fromUtf8("menuEdit"));
        menuView = new QMenu(menubar);
        menuView->setObjectName(QString::fromUtf8("menuView"));
        menuPage = new QMenu(menubar);
        menuPage->setObjectName(QString::fromUtf8("menuPage"));
        menuOptions = new QMenu(menubar);
        menuOptions->setObjectName(QString::fromUtf8("menuOptions"));
        MoeNavigatorWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MoeNavigatorWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MoeNavigatorWindow->setStatusBar(statusbar);

        menubar->addAction(menuNavigate->menuAction());
        menubar->addAction(menuEdit->menuAction());
        menubar->addAction(menuView->menuAction());
        menubar->addAction(menuPage->menuAction());
        menubar->addAction(menuOptions->menuAction());
        menuNavigate->addAction(actionOpen);
        menuNavigate->addAction(actionNext);
        menuNavigate->addAction(actionBackward);
        menuNavigate->addAction(actionQuit);
        menuEdit->addAction(actionCut);
        menuEdit->addAction(actionCopy);
        menuEdit->addAction(actionPaste);
        menuPage->addAction(actionAllow_Images);
        menuPage->addAction(actionAllow_JavaScript);
        menuPage->addAction(actionAllow_Audio);
        menuPage->addAction(actionAllow_Video);
        menuOptions->addAction(actionSettings);
        menuOptions->addAction(actionExtensions);

        retranslateUi(MoeNavigatorWindow);
        QObject::connect(actionQuit, SIGNAL(triggered()), MoeNavigatorWindow, SLOT(close()));

        QMetaObject::connectSlotsByName(MoeNavigatorWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MoeNavigatorWindow)
    {
        MoeNavigatorWindow->setWindowTitle(QApplication::translate("MoeNavigatorWindow", "MoeNavigator", 0, QApplication::UnicodeUTF8));
        actionOpen->setText(QApplication::translate("MoeNavigatorWindow", "Open", 0, QApplication::UnicodeUTF8));
        actionQuit->setText(QApplication::translate("MoeNavigatorWindow", "Quit", 0, QApplication::UnicodeUTF8));
        actionNext->setText(QApplication::translate("MoeNavigatorWindow", "Forward", 0, QApplication::UnicodeUTF8));
        actionBackward->setText(QApplication::translate("MoeNavigatorWindow", "Backward", 0, QApplication::UnicodeUTF8));
        actionAllow_Images->setText(QApplication::translate("MoeNavigatorWindow", "Allow Images", 0, QApplication::UnicodeUTF8));
        actionAllow_JavaScript->setText(QApplication::translate("MoeNavigatorWindow", "Allow JavaScript", 0, QApplication::UnicodeUTF8));
        actionAllow_Audio->setText(QApplication::translate("MoeNavigatorWindow", "Allow Audio", 0, QApplication::UnicodeUTF8));
        actionAllow_Video->setText(QApplication::translate("MoeNavigatorWindow", "Allow Video", 0, QApplication::UnicodeUTF8));
        actionSettings->setText(QApplication::translate("MoeNavigatorWindow", "Settings", 0, QApplication::UnicodeUTF8));
        actionExtensions->setText(QApplication::translate("MoeNavigatorWindow", "Extensions", 0, QApplication::UnicodeUTF8));
        actionCut->setText(QApplication::translate("MoeNavigatorWindow", "Cut", 0, QApplication::UnicodeUTF8));
        actionCopy->setText(QApplication::translate("MoeNavigatorWindow", "Copy", 0, QApplication::UnicodeUTF8));
        actionPaste->setText(QApplication::translate("MoeNavigatorWindow", "Paste", 0, QApplication::UnicodeUTF8));
        prevButton->setText(QApplication::translate("MoeNavigatorWindow", "<", 0, QApplication::UnicodeUTF8));
        nextButton->setText(QApplication::translate("MoeNavigatorWindow", ">", 0, QApplication::UnicodeUTF8));
        imagesButton->setText(QApplication::translate("MoeNavigatorWindow", "+Images", 0, QApplication::UnicodeUTF8));
        imagesButton_2->setText(QApplication::translate("MoeNavigatorWindow", "+JavaScript", 0, QApplication::UnicodeUTF8));
        imagesButton_3->setText(QApplication::translate("MoeNavigatorWindow", "+Multimedia", 0, QApplication::UnicodeUTF8));
        imagesButton_4->setText(QApplication::translate("MoeNavigatorWindow", "+Audio", 0, QApplication::UnicodeUTF8));
        imagesButton_5->setText(QApplication::translate("MoeNavigatorWindow", "+Video", 0, QApplication::UnicodeUTF8));
        menuNavigate->setTitle(QApplication::translate("MoeNavigatorWindow", "Navigate", 0, QApplication::UnicodeUTF8));
        menuEdit->setTitle(QApplication::translate("MoeNavigatorWindow", "Edit", 0, QApplication::UnicodeUTF8));
        menuView->setTitle(QApplication::translate("MoeNavigatorWindow", "View", 0, QApplication::UnicodeUTF8));
        menuPage->setTitle(QApplication::translate("MoeNavigatorWindow", "Page", 0, QApplication::UnicodeUTF8));
        menuOptions->setTitle(QApplication::translate("MoeNavigatorWindow", "Options", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MoeNavigatorWindow: public Ui_MoeNavigatorWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // MOENAVIGATORGUI_QT_H
