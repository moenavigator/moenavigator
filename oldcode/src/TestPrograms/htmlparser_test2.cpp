/*
    This file is part of MoeNavigator
    Copyright (C) 2012  Moritz Strohm <NCC1988@gmx.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "../MarkupParsers/HTMLParser.h"
#include "../CommonClasses/DocumentNode.h"

#include <iostream>

#include <stdio.h>


using namespace std;


int main(int argc, char *argv[])
{
  
  HTMLParser *parser = new HTMLParser();
  
  if(argc>=2)
  {
    //erstes Attribut gibt HTML-Datei an
    FILE *f = fopen(argv[1],"r");
    if(f == NULL)
    {
      cout << "Error: Testfile not found!" << endl;
      return 0;
    }
    
    //Testdatei lesen:
    fseeko64(f, 0L, SEEK_END);
    unsigned long fsize = ftell(f);
    fseeko64(f, 0L, SEEK_SET);
    
    char *htmlcode = new char[fsize];
    
    fread(htmlcode, 1, fsize, f);
    
    //cout << "HTMLCode: [" << htmlcode << "]" << endl << endl;
    
    parser->ParseFromString(htmlcode, fsize);
    
    //fclose(f);
  }
  else
  {
    string defaultcode = "<html><head><meta author=\"Moritz\"><title><!--Test-->HTMLParser Test 1</title></head><body style=\"background-color:black\"><div/><span>Test</span></body></html>\n ";
  
    parser->ParseFromString(defaultcode.c_str(), defaultcode.length());
  }
  
  
  
  
  DocumentNode *tree = parser->ReturnNodeTree();
  //MarkupNode *tree = parser->getTestPage();
  
  //cout << tree->ToJSON() << endl << endl;
  
  cout << tree->ToTree() << endl;
  
  
}