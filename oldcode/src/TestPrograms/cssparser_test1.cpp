/*
    This file is part of MoeNavigator
    Copyright (C) 2012  Moritz Strohm <NCC1988@gmx.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "../StylesheetParsers/CSSParser.h"

#include <iostream>

#include <stdio.h>


using namespace std;


int main(int argc, char *argv[])
{
  
  CSSParser *parser = new CSSParser();
  
  if(argc>=2)
  {
    //erstes Attribut gibt CSS-Datei an
    FILE *f = fopen(argv[1],"r");
    if(f == NULL)
    {
      cout << "Error: Testfile not found!" << endl;
      return 0;
    }
    
    //Testdatei lesen:
    fseeko64(f, 0L, SEEK_END);
    unsigned long fsize = ftell(f);
    fseeko64(f, 0L, SEEK_SET);
    
    char *cssstyles = new char[fsize];
    
    fread(cssstyles, 1, fsize, f);
    
    
    parser->ParseFromString(cssstyles, fsize);
    
    //fclose(f);
  }
  else
  {
    string defaultstyles = "/*Styles Test Data*/body{background-color:#FFFFFF;color:#000000;}";
  
    parser->ParseFromString(defaultstyles.c_str(), defaultstyles.length());
  }
  
  //cout << "Flushing parsed rules (destructor test)..." << endl;
  //parser->FlushStylesheetRules();
  
  
  StylesheetCollection* ParsedRules = parser->ReturnStylesheetRules();
  
  
  cout << "Parsed CSS Groups: " << ParsedRules->Groups.size() << endl;
  
  if(ParsedRules != NULL)
  {
    cout << ParsedRules->ToCSS() << endl;
  }
  else
  {
    cout << "ERROR: Parsed rules is a NULL pointer!" << endl;
  }
  
  cout << "Flushing parsed rules..." << endl;
  parser->FlushStylesheetRules();
  
  cout << "Deleting parsed rules..." << endl;
  
  delete ParsedRules;
  
  
  cout << "Trying to parse attributes in a HTML \"style\"-attribute..." << endl;
  
  string Attributes = "background-color: 20px; margin-top: 5px";
  parser->ParseFromString(Attributes.c_str(), Attributes.length(), STYLESHEET_STATE_RULEWHITE);
  
  ParsedRules = parser->ReturnStylesheetRules();
  
  if(ParsedRules != NULL)
  {
    cout << "Parsed CSS content: " << ParsedRules->ToCSS() << endl;
  }
  else
  {
    cout << "ERROR: Parsed rules is a NULL pointer!" << endl;
  }
  
  cout << "Deleting parsed rules..." << endl;
  
  delete ParsedRules;
  
  
  cout << "FINISHED!" << endl;
} 
