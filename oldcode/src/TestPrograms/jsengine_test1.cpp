/*
    This file is part of MoeNavigator
    Copyright (C) 2012  Moritz Strohm <NCC1988@gmx.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../ScriptEngines/JavaScriptEngine.h"

#include <iostream>

using namespace std;

class JSTest1CodeReader
{
  private:
    unsigned char readBlocks;
  
  public:
    char *ReadCodeBlock()
    {
      if(readBlocks==0)
      {
	readBlocks++;
	return (char*)"alert(\"Hallo Welt!\");";
      }
      else
      {
	return NULL;
      }
      
    }
    JSTest1CodeReader()
    {
      this->readBlocks = 0;
    }
};


int main(int argc, char *argv[])
{
  JavaScriptEngine *engine = new JavaScriptEngine();
  engine->ParseCodeFromString("alert(\"Hallo Welt!\");");
  
  javascript_token *tokenlist = engine->GetTokens();
}