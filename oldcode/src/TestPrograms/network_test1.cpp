/*
    This file is part of MoeNavigator
    Copyright (C) 2012  Moritz Strohm <NCC1988@gmx.net>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../MoeNavigatorEngine/MNENetwork/MNENetworkHandler.h"
#include "../MoeNavigatorEngine/MNENetwork/MNENetworkHandler_POSIX.h"
#include "../MoeNavigatorEngine/MNENetwork/MNENetProtocolHandler/MNENetProtocolHandler_HTTP.h"
#include "../MoeNavigatorEngine/MNECommon/MNECommon.h"

#include <iostream>
#include <string>

using namespace std;

int main(int argc, char *argv[])
{
  MNENetworkHandler* Network = new MNENetworkHandler_POSIX;
  MNENetProtocolHandler* HTTP = new MNENetProtocolHandler_HTTP(Network);
  
  HTTP->SetUserAgent("MoeNavigator network test application 1");
  
  string URL = "";
  
  if(argc>=2)
  {
    //first attribute contains an URL
    URL = argv[1];
  }
  else
  {
    URL = "http://127.0.0.1/index.html";
  }
  
  MNEURL TestURL(URL);
  long int Resource = HTTP->GetResource(TestURL);
  if(Resource < 0)
  {
    cout << "Error on opening the URL!" << endl;
    return 0;
  }
  
  cerr << "URL opened!" << endl << "Reading Data: " << endl;
  
  
  MNEDataPacket* Result = NULL;
  string ResultStr = "";
  do
  {
    try
    {
      Result = HTTP->ReadData(Resource,32768);
      if(Result != NULL)
      {
        ResultStr.append((char*)Result->Data, Result->Size);
        cerr << ResultStr;
        ResultStr = "";
      }
    }
    catch(MNEException e)
    {
      cerr << "Exception on reading: " << e.Component << ": " << e.Reason << endl;
    }
  }
  while(Result != NULL);
  
  
  /*Result = HTTP->ReadData(Resource,65536);
  if(Result != NULL)
  {
    if(Result->Size > 0)
    {
      cout << "Result->Size = " << Result->Size << endl;
      ResultStr.append((char*)Result->Data, Result->Size);
    }
  }*/
  
  
  cerr <</* ResultStr << */ endl << endl << "End of data. Header data:" << endl << "--------" << endl;
  MNEDataPacket* Header = HTTP->GetProtocolHeader(Resource);
  if(Header != NULL)
  {
    string HeaderStr = "";
    HeaderStr.append((char*)Header->Data, Header->Size);
    cerr << HeaderStr << endl;
  }
  cerr << "--------" << endl << "End of header data." << endl;
  
  HTTP->CloseResource(Resource);
  
  cerr << "URL closed!" << endl;
}