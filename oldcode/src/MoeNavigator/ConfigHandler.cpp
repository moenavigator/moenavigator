/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2015  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ConfigHandler.h"
#include <string>
#include <fstream>


ConfigHandler::ConfigHandler(std::string configFilePath)
{
  this->configuration = new ConfigSection("MoeNavigator")
  std::fstream configFile;
  configFile.open(configFilePath, std::fstream::in);
  std::string currentLine = "";
  while(!std::getline(configFile, currentLine).eof())
  {
    this->configuration.ParseKey(currentLine);
  }
}


std::string ConfigHandler::GetValue(std::string key)
{
  return this->configuration.GetValue(key);
}


void ConfigHandler::SetValue(std::string key, std::string value)
{
  this->configuration.SetValue(key, value);
}

std::string ConfigHandler::ToString()
{
  return this->configuration.ToString();
}