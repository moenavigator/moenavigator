/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2015  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONFIGHANDLER_H
#define CONFIGHANDLER_H

#include <string>
#include <vector>

struct ConfigSetting()
{
  std::string Key;
  std::string Value;
  
  ConfigSetting()
  {
    this->Key = "";
    this->Value = "";
  }
  
  ConfigSetting(std::string key, std::string value)
  {
    this->Key = key;
    this->Value = value;
  }
  
  std::string ToString(std::string prefix = "")
  {
    return prefix + this->Key + "=" + this->Value;
  }
};


class ConfigSection()
{
  public:
    std::string Name;
    vector<ConfigSection> SubSections;
    vector<std::string> Keys;
    
    ConfigSection()
    {
      this->Name = "";
      this->SubSections = new vector<ConfigSection>();
      this->Keys = new vector<std::string>();
    }
    
    std::string GetValue(std::string key)
    {
      for(uint32_t i = 0; i < this->Keys.size(); i++)
      {
	if(this->Keys[i].Key == key)
	{
	  return this->Keys[i].Value;
	}
      }
    }
    
    void SetValue(std::string key, std::string value)
    {
      for(uint32_t i = 0; i < this->Keys.size(); i++)
      {
	if(this->Keys[i].Key == key)
	{
	  this->Keys[i].Value = value;
	  return;
	}
      }
      //if this code is executed the key was not found -> add it!
      this->Keys.push_back(new ConfigSetting(key, value));
    }
    
    void ParseKey(std::string strKey = "")
    {
      std::stringstream unsplittedKey(strKey);
      std::string section;
      std::vector<std::string> keySections;

      while(std::getline(unsplittedKey, section, '.'))
      {
        keySections.push_back(section);
      }
      
      
      if(keySections[0] == "MoeNavigator")
      {
        //we're only interested in "MoeNavigator" keys and subsections:
        for(uint8_t i = 1; i < keySections.size(); i++)
        {
          
          keySections[i]
        }
      }
    }
    
    std::string ToString(std::string prefix = "")
    {
      std::string result = "";
      for(uint32_t i = 0; i < this->SubSections.size(); i++)
      {
	result += this->SubSections[i].ToString(this->Name);
      }
      
      for(uint32_t i = 0; i < this->Keys.size(); i++)
      {
	result += this->Keys[i].ToString(this->Name);
      }
    }
};

class ConfigHandler()
{
  private:
    ConfigSection configuration;
    
  public:
    ConfigHandler(std::string configFilePath);
    std::string GetValue(std::string key);
    void SetValue(std::string key, std::string value);
    
    std::string ToString();
};


#endif