/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2020  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QApplication>


#include <libconfig.h++>

#include <iostream>
#include <string>
#include <exception>
#include <cstdlib>
#include <sys/types.h>
#include <sys/stat.h>


#include "./DefaultSettings/DefaultPage.h"

#include "./EngineInterfaces/MoeNavigatorEngineInterface.h"


#include "./MoeNavigatorGUI/GUIHandler_Qt.h"

/* We access the browser engine via an interface.
 * This way we have a separation between the browser and the engine
 * and thus we can switch to another engine more easily.
 */


using namespace std;

string version = __DATE__;



MoeNavigatorEngineInterface* enginePtr = NULL;


/*
void stub_new_url_entered()
{
  Glib::ustring url = urlEntry->get_text();
  //cout << "URL string = " << url << endl;
  
  if(url.find("://") == std::string::npos)
  {
    //the protocol prefix was not specified.
    //Use https:// as default:
    url = "https://"+url;
    cout << "Changed url to " << url << endl;
  }
  enginePtr->OpenURL((std::string)url);
}
*/




int main(int argc, char* argv[])
{
  QApplication* app = new QApplication(argc, argv); // this has to be on top because otherwise you can't initialise the engine (with its QWidget)
  
  
  //read configuration:
  //create config directory:
  std::string baseDir = getenv("HOME");
  baseDir += "/.config/";
  mkdir(baseDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH);
  baseDir += "moenavigator/";
  mkdir(baseDir.c_str(), S_IRWXU | S_IRWXG | S_IROTH);
  
  libconfig::Config* navigatorConfig = new libconfig::Config();
  
  std::string configFile = baseDir + "moenavigator.cfg";
  try
  {
    navigatorConfig->readFile(configFile.c_str());
  }
  catch (exception e)
  {
    cerr << "Error: Can't read configuration file. Default configuration will be used!" << endl << endl;
    //use default configuration and write it to the configuration file:
    
  }
  
  
  
  cerr << "MoeNavigator  Copyright (C) 2012-2015  Moritz Strohm <ncc1988@posteo.de>" << endl
    << "This program comes with ABSOLUTELY NO WARRANTY." << endl
  // << "This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'." << endl
    << "This is free software, and you are welcome to redistribute it" << endl
    << "under certain conditions." << endl
  // << "under certain conditions; type `show c' for details." << endl
    << endl;

  //initialise GUI and Engine via GUIHandler:
    GUIHandler_Qt* gui = new GUIHandler_Qt();
    
    
    
//   //Engine initialisation:
// #ifdef USE_MOENAVIGATORENGINE
//   MoeNavigatorEngineInterface* engine = new MoeNavigatorEngineInterface();
// #endif
// #ifdef USE_WEBKIT
//   EngineInterface_QtWebKit* engine = new EngineInterface_QtWebKit();
// #endif
//   
//   
//   QUiLoader loader;
//   QFile uiFile("./src/MainGUI_QT.ui");
//   uiFile.open(QFile::ReadOnly);
//   
//   QWidget* mainWindow = loader.load(&uiFile);
//   uiFile.close();
//   
//   QWidget* engineContainer = mainWindow->findChild<QWidget*>("engineContainer");
//   QWidget* addressBar = mainWindow->findChild<QWidget*>("addressBar");
//   
//   
//   engine->SetParentWidget(engineContainer);
//   
//   //connect widget signals: FIXME
//   QObject::connect(addressBar, SIGNAL(returnPressed()),engine,SLOT(EngineInterface::OpenURL));
//   
//   
//   //show main window:
//   if(mainWindow != NULL)
//   {
//     mainWindow->show();
//   }
//   
//   //render default page:
//   engine->DrawMarkupString(defaultPage); //defaultPage is defined in ./src/DefaultSettings/DefaultPage.h
//   
  EngineInterface* engine = gui->GetEnginePointer();
  //render default page:
  engine->DrawMarkupString(defaultPage); //defaultPage is defined in ./src/DefaultSettings/DefaultPage.h
  
  return app->exec();
}
