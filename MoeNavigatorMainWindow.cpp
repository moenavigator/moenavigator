/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "MoeNavigatorMainWindow.h"


MoeNavigatorMainWindow::MoeNavigatorMainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MoeNavigatorMain)
{
    QApplication::setApplicationName("MoeNavigator");
    this->config_path = QDir::homePath() + "/.config/MoeNavigator/";

    //read the application settings:
    this->settings = std::make_shared<QSettings>(
        this->config_path + "MoeNavigator.ini",
        QSettings::IniFormat
    );

    this->ui->setupUi(this);
    this->config_window = std::make_shared<ConfigurationWindow>();
    this->about_dialog = std::make_shared<MoeNavigatorAboutDialog>();

    this->ui_handler = std::make_shared<GUIHandler>();

    //Initially hide the browser console:
    this->ui->consoleTab->hide();


    //connect signals:

    //from GUI to GUI or engine:
    //configuration window:
    this->connect(
        this->ui->actionSettings,
        SIGNAL(triggered()),
        this,
        SLOT(showConfigWindow())
    );
    //about dialog:
    this->connect(
        this->ui->actionAbout,
        SIGNAL(triggered()),
        this,
        SLOT(showAboutDialog())
    );
    this->connect(
        this->about_dialog.get(),
        SIGNAL(URLClicked(QString)),
        this,
        SLOT(openNewTab(QString))
    );

    //address bar:
    this->connect(
        this->ui->addressBar,
        SIGNAL(returnPressed()),
        this,
        SLOT(loadNewURL())
    );

    //address bar buttons:
    this->connect(
        this->ui->prevButton,
        SIGNAL(clicked()),
        this,
        SLOT(previousURL())
    );
    this->connect(
        this->ui->nextButton,
        SIGNAL(clicked()),
        this,
        SLOT(nextURL())
    );
    this->connect(
        this->ui->reloadButton,
        SIGNAL(clicked()),
        this,
        SLOT(reloadPage())
    );
    this->connect(
        this->ui->newTabButton,
        SIGNAL(clicked()),
        this,
        SLOT(openNewTab())
    );

    //menu items:
    this->connect(
        this->ui->actionMoeNavigatorEngine,
        SIGNAL(toggled(bool)),
        this,
        SLOT(switchEngine(bool))
        );

    this->connect(
        this->ui->actionShow_console,
        SIGNAL(toggled(bool)),
        this,
        SLOT(toggleConsole(bool))
        );

    //page buttons:
    this->connect(
        this->ui->torToggleButton,
        SIGNAL(toggled(bool)),
        this,
        SLOT(updateTorStatus(bool))
    );
    this->connect(
        this->ui->imagesToggleButton,
        SIGNAL(toggled(bool)),
        this,
        SLOT(updateImageDisplayStatus(bool))
    );
    this->connect(
        this->ui->scriptToggleButton,
        SIGNAL(toggled(bool)),
        this,
        SLOT(updateJavaScriptStatus(bool))
    );
    this->connect(
        this->ui->cookiesToggleButton,
        SIGNAL(toggled(bool)),
        this,
        SLOT(updateCookieStatus(bool))
    );
    this->connect(
        this->ui->radioButtonDomain,
        SIGNAL(toggled(bool)),
        this,
        SLOT(toggleDomainSettings(bool))
    );

    //from engine to GUI (the reverse way):
    this->connect(
        this->ui_handler.get(),
        SIGNAL(URLChanged(QObject*, QUrl)),
        this,
        SLOT(updateURLFields(QObject*, QUrl))
    );
    this->connect(
        this->ui_handler.get(),
        SIGNAL(URLChanged(QObject*, QUrl)),
        this,
        SLOT(updatePageButtons())
    );
    this->connect(
        this->ui_handler.get(),
        SIGNAL(URLHovered(QString)),
        this->ui->statusbar,
        SLOT(showMessage(QString))
    );
    this->connect(
        this->ui_handler.get(),
        SIGNAL(LoadStatusChanged(QObject*,GUIHandler::LoadingStatus)),
        this,
        SLOT(updateLoadingStatus(QObject*,GUIHandler::LoadingStatus))
    );
    this->connect(
        this->ui_handler.get(),
        SIGNAL(NewTabRequest(QString)),
        this,
        SLOT(openNewTab(QString))
    );

    //Set up the tab widget with a new browser tab:
    this->tab_widget = new QTabWidget(ui->tabWidgetContainer);
    if (!this->tab_widget.isNull()) {
        this->tab_widget->setTabsClosable(true);
        this->ui->tabWidgetLayout->addWidget(this->tab_widget);
        //Connect tab wiget signals:
        this->connect(
            this->tab_widget,
            SIGNAL(currentChanged(int)),
            this,
            SLOT(switchTab())
            );
        this->connect(
            this->tab_widget,
            SIGNAL(tabCloseRequested(int)),
            this,
            SLOT(closeTab(int))
            );
        this->openNewTab();
    }
}


void MoeNavigatorMainWindow::openNewTab(QString tab_url)
{
    this->openNewTab();
    if (tab_url != "") {
        this->current_tab->engine->OpenURL(tab_url);
    }
}


void MoeNavigatorMainWindow::openNewTab()
{
    BrowserTabWidget* new_tab = new BrowserTabWidget(
        this->tab_widget,
        this->ui_handler,
        "mne",
        this->ui->domTreeWidget
        );

    this->tab_widget->addTab(
        new_tab,
        "(empty)"
        );

    this->current_tab = new_tab;
    this->current_tab->show();
}


void MoeNavigatorMainWindow::openUrl(QString url)
{
    //First we have to check if this is an URL:
    //No white spaces allowed!
    if (!url.contains(" ")) {
        if (!url.contains("://")) {
            //No protocol specified. Place https:// in front of it:
            url = "https://" + url;
            this->current_tab->url_text = url;
        }
    } else {
        //The URL is not an URL at all. We have a search request:
        this->search(url);
    }

    if (this->current_tab->engine != nullptr) {
        this->current_tab->engine->OpenURL(url);
    }
    this->current_tab->url_text = url;
}


void MoeNavigatorMainWindow::search(QString url)
{
    //TODO: build search engine configuration module!
    //it has to include the choice betweeen HTTP GET and POST (startpage.com uses POST)

    if (this->current_tab->engine != nullptr) {
        this->current_tab->engine->OpenURL("https://startpage.com/do/search", "cmd=process_search&query="+url);
    }
}


void MoeNavigatorMainWindow::loadMarkup(std::string markup)
{
    if (this->current_tab->engine != nullptr) {
        this->current_tab->engine->DrawMarkupString(markup);
    }
}


void MoeNavigatorMainWindow::switchTab()
{
    if (this->tab_widget.isNull()) {
        return;
    }

    //first set the new current tab:
    this->current_tab = dynamic_cast<BrowserTabWidget*>(
        this->tab_widget->currentWidget()
        );
    if (current_tab.isNull()) {
        //The tab is invalid.
        return;
    }

    //Now set the address bar:
    this->ui->addressBar->setText(this->current_tab->url_text);

    if (this->current_tab->engine != nullptr) {
        //set the window title:
        QString new_title = this->current_tab->engine->GetCurrentTitle();
        if(new_title.length() > 64) {
            this->setWindowTitle(new_title.left(64) + "... - MoeNavigator");
        } else {
            this->setWindowTitle(new_title + " - MoeNavigator");
        }

        //and set the quick settings buttons:
        this->ui->torToggleButton->setChecked(
            this->current_tab->engine->GetStatus(
                EngineInterface::STATUS_TOR_ENABLED
                )
            );
        this->ui->imagesToggleButton->setChecked(
            this->current_tab->engine->GetStatus(
                EngineInterface::STATUS_IMAGES_ENABLED
                )
            );
        this->ui->scriptToggleButton->setChecked(
            this->current_tab->engine->GetStatus(
                EngineInterface::STATUS_JAVASCRIPT_ENABLED
                )
            );
        this->ui->cookiesToggleButton->setChecked(
            this->current_tab->engine->GetStatus(
                EngineInterface::STATUS_STORAGE_ENABLED
                )
            );
    }
}


void MoeNavigatorMainWindow::closeTab(int tab_id)
{
    if (this->tab_widget.isNull()) {
        return;
    }

    QPointer<BrowserTabWidget> closing_tab = qobject_cast<BrowserTabWidget*>(
        this->tab_widget->widget(tab_id)
        );
    if (closing_tab.isNull()) {
        //no such tab.
        return;
    }

    delete closing_tab;

    //Set the current_tab pointer to another tab, if any:

    this->current_tab = qobject_cast<BrowserTabWidget*>(
        this->tab_widget->currentWidget()
        );

    if (this->current_tab.isNull()) {
        //No tab available: close the browser
        this->close();
    }
}


void MoeNavigatorMainWindow::updateTorStatus(bool enabled)
{
    if (this->current_tab->engine != nullptr) {
        this->current_tab->engine->SetTorStatus(enabled);
    }
}


void MoeNavigatorMainWindow::updateImageDisplayStatus(bool enabled)
{
    if (this->current_tab->engine != nullptr) {
        this->current_tab->engine->SetImageDisplayStatus(enabled);
    }
}


void MoeNavigatorMainWindow::updateJavaScriptStatus(bool enabled)
{
    if (this->current_tab->engine != nullptr) {
        this->current_tab->engine->SetScriptEngineStatus(enabled);
    }
}


void MoeNavigatorMainWindow::updateCookieStatus(bool enabled)
{
    if (this->current_tab->engine != nullptr) {
        this->current_tab->engine->SetWebsiteStorageStatus(enabled);
    }
}


void MoeNavigatorMainWindow::updateWebStorageStatus(bool enabled)
{
    if (this->current_tab->engine != nullptr) {
        this->current_tab->engine->SetCookieStatus(enabled);
        this->current_tab->engine->SetLocalStorageStatus(enabled);
    }
}


void MoeNavigatorMainWindow::toggleDomainSettings(bool enabled)
{
    if (this->current_tab->engine != nullptr) {
        this->current_tab->engine->UseDomainSpecificSettings(enabled);
    }
    this->updatePageButtons();
}


MoeNavigatorMainWindow::~MoeNavigatorMainWindow()
{
    delete this->tab_widget;
    delete this->ui;
}


void MoeNavigatorMainWindow::loadNewURL()
{
    this->openUrl(this->ui->addressBar->text());
}


/*
void MoeNavigatorMainWindow::UpdateTitleFields(QObject* newTitleSender, QString newTitle)
{
    //we have to make sure that the title for the main window is only set
    //when the title has changed in the current (active) tab:
    if(this->current_tab->engine->EngineIsObject(newTitleSender))
    {
        if(newTitle.length() > 64)
        {
            this->setWindowTitle(newTitle.left(64) + "... - MoeNavigator");
        }
        else
        {
            this->setWindowTitle(newTitle + " - MoeNavigator");
        }
        //also, if it is the current tab we don't have to search all the tabs
        //to find out from which tab the sending QObject is from.
        //Thus we can set the current tab's title directly:
        int currentTabIndex = ui->browserTabs->indexOf(this->current_tab->widget);
        if(currentTabIndex > -1)
        {
            if(newTitle.length() > 48)
            {
                ui->browserTabs->setTabText(currentTabIndex, newTitle.left(48) + "...");
            }
            else
            {
                ui->browserTabs->setTabText(currentTabIndex, newTitle);
            }
        }
    }
    else
    {
        //the title of an inactive (not the current) tab has changed:
        //we have to walk over all tabs and check if the sender is an engine
        //inside one of those tabs.
        QWidget* foundTabWidget = NULL;
        for(size_t i = 0; i < this->tabs.size(); i++)
        {
            if(this->tabs[i]->engine->EngineIsObject(newTitleSender))
            {
                foundTabWidget = this->tabs[i]->widget;
            }
        }
        int tabIndex = ui->browserTabs->indexOf(foundTabWidget);
        if(tabIndex > -1)
        {
            ui->browserTabs->setTabText(tabIndex, newTitle);
        }
    }
}
*/


void MoeNavigatorMainWindow::updateURLFields(QObject* sender, QUrl new_url)
{
    if (this->current_tab->engine == nullptr) {
        return;
    }

    //Check if updated URL is coming from current tab. If so,
    //update the address bar text:
    if (this->current_tab->engine->EngineIsObject(sender)) {
        this->ui->addressBar->setText(new_url.toString());
    }

    //set the URL text attribute, no matter from which tab the sender comes from:
    /*
    for(size_t i = 0; i < this->tabs.size(); i++)
    {
        if(this->tabs[i]->engine->EngineIsObject(sender))
        {
            //TODO: add a check: only set the new text if the new URL was set without problems
            //this should avoid a blank URL when a page cannot be loaded.
            this->tabs[i]->url_text = new_url.toString();
        }
    }
    */
}


void MoeNavigatorMainWindow::updateStatusBar(QString new_url, QString new_title, QString new_content)
{
    this->ui->statusbar->setStatusTip(new_url);
}


void MoeNavigatorMainWindow::updateLoadingStatus(
    QObject *sender,
    GUIHandler::LoadingStatus loading_status
    )
{
    if (this->current_tab->engine == nullptr) {
        return;
    }

    if (this->current_tab->engine->EngineIsObject(sender))
    {
        QString loading_label_text = "";
        //set the loading label:
        if (loading_status == GUIHandler::LOAD_STATUS_STARTED) {
            loading_label_text = "Loading...";
        } else if (loading_status == GUIHandler::LOAD_STATUS_ERROR) {
            loading_label_text = "Error loading page!";
        }
        this->ui->loadingStatusLabel->setText(loading_label_text);
    }
}


void MoeNavigatorMainWindow::switchEngine(bool use_mne)
{
    if (use_mne) {
        this->current_tab->setEngine("mne");
    } else {
        this->current_tab->setEngine("webkit");
    }
    if (this->current_tab->engine != nullptr) {
        this->current_tab->engine->OpenURL(this->current_tab->url_text);
    }
}


void MoeNavigatorMainWindow::updatePageButtons()
{
    if (this->current_tab->engine == nullptr) {
        return;
    }

    if(this->current_tab->engine->SettingsAreDomainSpecific())
    {
        this->ui->radioButtonDomain->setChecked(true);
    }
    else
    {
        this->ui->radioButtonDomain->setChecked(false);
    }

    this->ui->torToggleButton->setChecked(
        this->current_tab->engine->GetStatus(EngineInterface::STATUS_TOR_ENABLED)
    );
    this->ui->imagesToggleButton->setChecked(
        this->current_tab->engine->GetStatus(EngineInterface::STATUS_IMAGES_ENABLED)
    );
    this->ui->scriptToggleButton->setChecked(
        this->current_tab->engine->GetStatus(EngineInterface::STATUS_JAVASCRIPT_ENABLED)
    );
    this->ui->cookiesToggleButton->setChecked(
        this->current_tab->engine->GetStatus(EngineInterface::STATUS_STORAGE_ENABLED)
    );
}


void MoeNavigatorMainWindow::nextURL()
{
    if (this->current_tab->engine != nullptr) {
        this->current_tab->engine->NextURL();
    }
}

void MoeNavigatorMainWindow::previousURL()
{
    if (this->current_tab->engine != nullptr) {
        this->current_tab->engine->PreviousURL();
    }
}


void MoeNavigatorMainWindow::reloadPage()
{
    if (this->current_tab->engine != nullptr) {
        QString current_url = this->current_tab->engine->GetCurrentURL();
        this->current_tab->engine->OpenURL(current_url);
    }
}


void MoeNavigatorMainWindow::toggleConsole(bool enabled)
{
    if (this->current_tab != nullptr) {
        this->current_tab->setConsoleEnabled(enabled);
    }
}


void MoeNavigatorMainWindow::showConfigWindow()
{
    this->config_window->show();
}


void MoeNavigatorMainWindow::showAboutDialog()
{
    this->about_dialog->show();
}


void MoeNavigatorMainWindow::updateConfiguration()
{
    //TODO: check config_window GUI variables and set the corresponding configuration options
    //this->engine->SetConfiguration();
}
