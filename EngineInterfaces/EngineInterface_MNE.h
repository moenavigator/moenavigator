/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENGINEINTERFACE_MNE_H
#define ENGINEINTERFACE_MNE_H


#include <memory>
#include <string>

#include <QSvgWidget>
#include <QObject>
#include <QWidget>


#include "EngineInterface.h"
#include "MoeNavigatorEngine/MoeNavigatorEngine.h"
#include "MoeNavigatorEngine/MNERenderer/MNERenderer_SVG.h"
#include "MoeNavigatorEngine/MNERenderer/MNERenderedData.h"


/**
 * The MoeNavigatorEngineInterface class provides an interface
 * for MoeNavigatorEngine.
 */
class EngineInterface_MNE : public EngineInterface
{

    public:


    EngineInterface_MNE(QPointer<QWidget> parent);


    ~EngineInterface_MNE();


    void setGUIHandler(GUIHandler* gui_handler);


    GUIHandler* getGUIHandler();


    bool EngineIsObject(QObject* other);


    bool SettingsAreDomainSpecific();


    void UseDomainSpecificSettings(bool enabled = false);


    void SetImageDisplayStatus(bool enabled);


    void SetScriptEngineStatus(bool enabled = false);


    void SetStyleSheetParserStatus(bool enabled = true);


    void SetWebsiteStorageStatus(bool enabled = false);


    void SetCookieStatus(bool enabled = false);


    void SetLocalStorageStatus(bool enabled = false);


    void SetTorStatus(bool enabled);


    bool GetStatus(EngineStatus status);


    //Graphic methods:


    void DrawPage();


    void DrawMarkupString(std::string markup);


    void setParentWidget(QPointer<QWidget> parent);


    //Network:


    void OpenURL(QString url);


    void OpenURL(QString url, QString post_data);


    void PreviousURL();


    void NextURL();


    void setUserAgent(std::string user_agent);


    QString GetCurrentURL();


    QString GetCurrentTitle();


    std::shared_ptr<MoeNavigatorEngine> getEngine();


    protected:


    QPointer<QWidget> parent_widget;


    std::shared_ptr<MoeNavigatorEngine> engine_instance;


    std::shared_ptr<MNERenderer> svg_renderer;


    GUIHandler* gui_handler;


    QPointer<QSvgWidget> svg_widget;
};


#endif
