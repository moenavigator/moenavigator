/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2022  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ENGINEINTERFACE_H
#define ENGINEINTERFACE_H


#include <string>

#include <QtWidgets>
#include <QObject>
#include <QString>

#include "../GUIHandler.h"
#include "../EngineSettings.h"


/**
 * The EngineInterface class is an abstraction layer between the application and
 * the used browser engine. This way several browser engines can be used
 * in the application.
 */
class EngineInterface
{

    public:


    enum EngineStatus
    {
        STATUS_TOR_ENABLED,

        STATUS_IMAGES_ENABLED,

        STATUS_JAVASCRIPT_ENABLED,

        STATUS_STORAGE_ENABLED
    };


    virtual ~EngineInterface(){}


    //GUI interaction:


    virtual void setGUIHandler(GUIHandler* gui_handler) = 0;


    virtual GUIHandler* getGUIHandler() = 0;


    //other stuff:


    virtual bool EngineIsObject(QObject* other) = 0;


    //Configuration:


    virtual bool SettingsAreDomainSpecific() = 0;


    virtual void UseDomainSpecificSettings(bool enabled = false) = 0;


    virtual void SetImageDisplayStatus(bool enabled) = 0;


    /**
     * @param enabled set this to "true" to enable script engines (JavaScript),
     * otherwise they will be disabled (the default). Set the script engine
     * (JavaScript) status with this to enable or disable these engines.
     */
    virtual void SetScriptEngineStatus(bool enabled = false) = 0;


    /**
     * @param enabled set this to "false" to disable stylesheet parsers (CSS),
     * otherwise they will be enabled (the default). Set the stylesheet parser
     * (CSS) status with this to enable or disable these parsers.
     */
    virtual void SetStyleSheetParserStatus(bool enabled = true) = 0;


    /**
     * This enables or disables cookies, session storage and local storage
     * (default: disabled).
     *
     * @param enabled true means cookies, session storage and local storage are
     *     enabled, the default is false.
     */
    virtual void SetWebsiteStorageStatus(bool enabled = false) = 0;


    virtual void SetCookieStatus(bool enabled = false) = 0;


    virtual void SetLocalStorageStatus(bool enabled = false) = 0;


    virtual void SetTorStatus(bool enabled) = 0;


    virtual bool GetStatus(EngineStatus status) = 0;


    //Graphic functions:


    /**
     * DrawPage is called when a page has been loaded and the DOM structure has
     * been created. This function may be called by the engine itself or by
     * the application that uses the engine.
     */
    virtual void DrawPage() = 0;


    /**
     * DrawMarkupString can be used to render a XHTML, SVG, XML or HTML string.
     * This is useful if the markup code is generated inside the browser
     * (e.g. default start page, settings page, ...)
     *
     * @param markup A string that contains code in a markup language the engine
     *     understands.
     */
    virtual void DrawMarkupString(std::string markup) = 0;


    virtual void setParentWidget(QPointer<QWidget> parent) = 0;


    //Network:


    /**
     * @param URL A string containing a URL
     * OpenURL tries to open the URL specified.
     */
    virtual void OpenURL(QString url) = 0;


    /**
     * @brief OpenURL This variant of OpenURL allows sending HTTP POST data
     * @param url The URL to be opened (and optional HTTP GET parameters)
     * @param post_data HTTP POST data to be sent
     */
    virtual void OpenURL(QString url, QString post_data) = 0;


    /**
     * This represents the action when a user presses the "back" button in the
     * browser window
     */
    virtual void PreviousURL() = 0;


    /**
     * This represents the "forward" button, after a uses has pressed the
     * "back" button.
     */
    virtual void NextURL() = 0;


    /**
     * SetUserAgent sets the user agent string. The specified string will be
     * used in HTTP request headers.
     *
     * @param user_agent A user agent string
     */
    virtual void setUserAgent(std::string user_agent) = 0;


    virtual QString GetCurrentURL() = 0;


    virtual QString GetCurrentTitle() = 0;
};


#endif
