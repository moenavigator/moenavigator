/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "EngineInterface_MNE.h"


EngineInterface_MNE::EngineInterface_MNE(QPointer<QWidget> parent)
{
    this->parent_widget = parent;
    this->engine_instance = std::shared_ptr<MoeNavigatorEngine>(
        new MoeNavigatorEngine()
        );
    //TODO: set document dimensions from the current window dimensions.
    this->engine_instance->setDocumentDimensions(800, 600);
    this->svg_renderer = std::shared_ptr<MNERenderer>(
        new MNERenderer_SVG()
        );
    this->engine_instance->addRenderer(this->svg_renderer);
    this->svg_widget = new QSvgWidget();
    this->svg_widget->setSizePolicy(
        QSizePolicy::Expanding,
        QSizePolicy::Expanding
        );
    this->svg_widget->setLayout(new QGridLayout());
    if (!parent.isNull()) {
        this->svg_widget->setParent(parent);
    }
    this->svg_widget->show();
    this->gui_handler = nullptr;
}


EngineInterface_MNE::~EngineInterface_MNE()
{
    //parent_widget isn't owned by the engine interface,
    //so it isn't deallocated here.
    this->parent_widget = 0;
    //The QSvgWidget on the other hand is owned by this engine interface.
    delete this->svg_widget;

    this->engine_instance = nullptr;
}


void EngineInterface_MNE::setGUIHandler(GUIHandler* gui_handler)
{
    this->gui_handler = gui_handler;
}


GUIHandler* EngineInterface_MNE::getGUIHandler()
{
    return this->gui_handler;
}


bool EngineInterface_MNE::EngineIsObject(QObject* other)
{
    return false; //TODO
}


bool EngineInterface_MNE::SettingsAreDomainSpecific()
{
    return false; //TODO
}


void EngineInterface_MNE::UseDomainSpecificSettings(bool enabled)
{
    //TODO
}


void EngineInterface_MNE::SetImageDisplayStatus(bool enabled)
{
    //TODO
}


void EngineInterface_MNE::SetScriptEngineStatus(bool enabled)
{
    //TODO
}


void EngineInterface_MNE::SetStyleSheetParserStatus(bool enabled)
{
    //TODO
}


void EngineInterface_MNE::SetWebsiteStorageStatus(bool enabled)
{
    //TODO
}


void EngineInterface_MNE::SetCookieStatus(bool enabled)
{
    //TODO
}


void EngineInterface_MNE::SetLocalStorageStatus(bool enabled)
{
    //TODO
}


void EngineInterface_MNE::SetTorStatus(bool enabled)
{
    //TODO
}


bool EngineInterface_MNE::GetStatus(EngineStatus status)
{
    return true; //TODO
}


//Graphic functions:


void EngineInterface_MNE::DrawPage()
{
  this->engine_instance->drawPage();
}


void EngineInterface_MNE::DrawMarkupString(std::string markup)
{
    //TODO
}


void EngineInterface_MNE::setParentWidget(QPointer<QWidget> parent)
{
    if(!parent.isNull()) {
        this->parent_widget = parent;
        this->svg_widget->setParent(parent);
    }
}


//Network:


void EngineInterface_MNE::OpenURL(QString url)
{
    if (url.size() > 0) {
        this->gui_handler->EmitLoadStart();
        this->engine_instance->openURL(url.toStdString());
        this->engine_instance->drawPage();
        std::shared_ptr<MNERenderedData> data =
            this->svg_renderer->getRenderedData();
        if (data == nullptr) {
            //Show a blank page:
            this->svg_widget->load(QString(""));
        } else {
            this->svg_widget->load(
                QByteArray(
                    data->data.c_str()
                    )
                );
        }
        this->gui_handler->EmitLoadFinished(true);
    }
}


void EngineInterface_MNE::OpenURL(QString url, QString post_data)
{
    if (url.size() > 0) {
        this->OpenURL(url);
        //TODO: handle POST data
    }
}


void EngineInterface_MNE::PreviousURL()
{
    //TODO
}


void EngineInterface_MNE::NextURL()
{
    //TODO
}


void EngineInterface_MNE::setUserAgent(std::string user_agent)
{
    this->engine_instance->setUserAgent(user_agent);
}


QString EngineInterface_MNE::GetCurrentURL()
{
    //TODO
    return "";
}


QString EngineInterface_MNE::GetCurrentTitle()
{
    //TODO
    return "";
}


std::shared_ptr<MoeNavigatorEngine> EngineInterface_MNE::getEngine()
{
    return this->engine_instance;
}
