/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GUIHANDLER_H
#define GUIHANDLER_H


#include <iostream>


#include <QObject>
#include <QUrl>
#include <QAction>




class GUIHandler : public QObject
{
    Q_OBJECT


    public:


    enum LoadingStatus
    {
        LOAD_STATUS_STARTED,

        LOAD_STATUS_ABORTED,

        LOAD_STATUS_FINISHED,

        LOAD_STATUS_ERROR
    };


    explicit GUIHandler(QObject *parent = 0);


    signals:


    void URLChanged(QObject*, QUrl);


    void TitleChanged(QObject*, QString);


    void URLHovered(QString);


    void StatusBarTextChanged(QString);


    void LoadStatusChanged(QObject*, GUIHandler::LoadingStatus);


    void NewTabRequest(QString);


    public slots:


    void EmitNewTabRequest(QString url);


    void EmitNewTabRequestFromQAction();


    void EmitURLChange(QUrl newURL);


    void EmitTitleChange(QString newTitle);


    void EmitURLHover(QString url, QString title, QString content);


    void EmitStatusBarText(QString newText);


    void EmitLoadStart();


    void EmitLoadFinished(bool success);
};


#endif
