/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef MOENAVIGATORMAIN_H
#define MOENAVIGATORMAIN_H

#include <QMainWindow>
#include <QSettings>
#include <QPointer>

#include "ConfigurationWindow.h"
#include "MoeNavigatorAboutDialog.h"
#include "ui_MoeNavigatorMainWindow.h"

#include "GUIHandler.h"
#include "BrowserTabWidget.h"

#include <iostream>
#include <memory>


namespace Ui
{
    class MoeNavigatorMain;
}


class MoeNavigatorMainWindow : public QMainWindow
{
    Q_OBJECT


    public:


    explicit MoeNavigatorMainWindow(QWidget *parent = 0);


    MoeNavigatorMainWindow(QString url);


    ~MoeNavigatorMainWindow();


    void openUrl(QString url);


    void search(QString url);


    void loadMarkup(std::string markup);


    public slots:


    /**
     * A slot for opening the configuration window.
     */
    void showConfigWindow();


    /**
     * A slot for opening the about window.
     */
    void showAboutDialog();


    void updateConfiguration();


    /**
     * Opens a new tab with an URL.
     *
     * @param QString tab_url The URL to open in the new tab.
     */
    void openNewTab(QString tab_url);


    /**
     * Opens an empty new tab.
     */
    void openNewTab();


    void loadNewURL();


    void updateURLFields(QObject* sender, QUrl new_url);


    //void UpdateTitleFields(QObject* newTitleSender, QString newTitle);


    void updateStatusBar(QString new_url, QString new_title, QString new_content);


    void updateLoadingStatus(
        QObject* sender,
        GUIHandler::LoadingStatus loading_status
        );


    /**
     * Switches the browser engine.
     *
     * @param bool use_mne Whether to use MoeNavigatorEngine (true)
     *     or WebKit using QtWebkit (false).
     */
    void switchEngine(bool use_mne);


    //TODO: rewrite content for multi-tab support
    //and for page-specific configurations
    void updatePageButtons();


    void updateTorStatus(bool enabled);


    void updateImageDisplayStatus(bool enabled);


    void updateJavaScriptStatus(bool enabled);


    void updateCookieStatus(bool enabled);


    void updateWebStorageStatus(bool enabled);


    void toggleDomainSettings(bool enabled);


    void switchTab();


    void closeTab(int tab_id);


    /**
     * Navigates to the next URL in the URL history.
     */
    void nextURL();


    /**
     * Navigates to the previously visited URL from the URL history.
     */
    void previousURL();


    /**
     * Triggers the reload of the currently visited page.
     */
    void reloadPage();


    /**
     * Toggles the visibility of the browser console.
     */
    void toggleConsole(bool enabled);


    protected:


    void initApplication();


    Ui::MoeNavigatorMain* ui;


    std::shared_ptr<ConfigurationWindow> config_window = nullptr;


    std::shared_ptr<MoeNavigatorAboutDialog> about_dialog = nullptr;


    std::vector<std::shared_ptr<BrowserTabWidget>> tabs;


    QPointer<BrowserTabWidget> current_tab = nullptr;


    std::shared_ptr<GUIHandler> ui_handler = nullptr;;


    std::shared_ptr<QSettings> settings = nullptr;


    QString config_path;


    QPointer<QTabWidget> tab_widget;
};


#endif
