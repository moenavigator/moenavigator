/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CONFIGURATIONWINDOW_H
#define CONFIGURATIONWINDOW_H


#include <QWidget>


namespace Ui
{
    class ConfigurationWindow;
}


class ConfigurationWindow : public QWidget
{
    Q_OBJECT


    public:


    explicit ConfigurationWindow(QWidget *parent = 0);


    ~ConfigurationWindow();


    protected:


    Ui::ConfigurationWindow *ui;
};


#endif
