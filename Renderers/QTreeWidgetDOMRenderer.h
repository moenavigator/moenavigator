/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef QTREEWIDGETDOMRENDERER_H
#define QTREEWIDGETDOMRENDERER_H


#include <QTreeWidget>
#include <QTreeWidgetItem>
#include "MoeNavigatorEngine/MNERenderer/MNERenderer.h"
#include "MoeNavigatorEngine/CommonClasses/DocumentNode.h"


class QTreeWidgetDOMRenderer: public MNERenderer
{
    protected:


    /**
     * The document from which to create a DOM.
     */
    std::shared_ptr<DocumentNode> document;


    /**
     * A (non-owning) pointer to the QTreeWidget
     * that displays the DOM.
     */
    QTreeWidget* tree_widget = nullptr;


    /**
     * Produces QTreeWidgetItem instances from a node and its children
     * and appends them as children to the specified parent QTreeWidgetItem.
     */
    void nodeToItem(std::shared_ptr<DocumentNode> node, QTreeWidgetItem* parent);


    public:


    /**
     * @see MNERenderer class
     */
    virtual uint32_t getSupportedOutputModes() override;


    /**
     * @see MNERenderer class
     */
    virtual void setOutputMode(MNERenderer::OutputMode mode) override;


    /**
     * @see MNERenderer class
     */
    virtual void setDrawingAreaDimensions(uint32_t width, uint32_t height) override;


    /**
     * @see MNERenderer class
     */
    virtual void setDocument(std::shared_ptr<DocumentNode> document) override;


    /**
     * @see MNERenderer class
     */
    virtual std::shared_ptr<MNERenderedData> getRenderedData() override;


    /**
     * @see MNERenderer class
     */
    virtual void render() override;


    virtual void setTreeWidget(QTreeWidget* tree_widget);


    virtual QTreeWidget* getTreeWidget();
};


#endif
