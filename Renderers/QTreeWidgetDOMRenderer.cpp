/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "QTreeWidgetDOMRenderer.h"


void QTreeWidgetDOMRenderer::nodeToItem(std::shared_ptr<DocumentNode> node, QTreeWidgetItem* node_item)
{
    if ((node == nullptr) || (node_item == nullptr)) {
        //Nothing to do.
        return;
    }

    for (auto child: node->children) {
        auto child_item = new QTreeWidgetItem(node_item, QTreeWidgetItem::Type);
        if (child_item == nullptr) {
            //We cannot continue... well, we cannot continue processing
            //the child node.
            continue;
        }
        std::string child_attribute_text = "";
        for (auto attribute: child->attributes) {
            child_attribute_text += attribute.first + "=\"" + attribute.second + "\" ";
        }

        child_item->setText(0, child->name.c_str());
        child_item->setText(1, child_attribute_text.c_str());
        child_item->setExpanded(true);
        this->nodeToItem(child, child_item);
    }
}


uint32_t QTreeWidgetDOMRenderer::getSupportedOutputModes()
{
    return MNERenderer::OutputMode::DOM;
}


void QTreeWidgetDOMRenderer::setOutputMode(MNERenderer::OutputMode mode)
{
    //Only DOM output mode is supported.
}


void QTreeWidgetDOMRenderer::setDrawingAreaDimensions(uint32_t width, uint32_t height)
{
    //Unneeded, since only the DOM tree is displayed.
}


void QTreeWidgetDOMRenderer::setDocument(std::shared_ptr<DocumentNode> document)
{
    this->document = document;
}


std::shared_ptr<MNERenderedData> QTreeWidgetDOMRenderer::getRenderedData()
{
    //Not applicable, since to rendered output is created.
    return nullptr;
}


void QTreeWidgetDOMRenderer::render()
{
    if ((this->tree_widget == nullptr) || (this->document == nullptr)) {
        //Nothing to do.
        return;
    }

    //Clear the content of the widget before rendering a new document:
    this->tree_widget->clear();

    auto document_item = new QTreeWidgetItem(this->tree_widget, QTreeWidgetItem::Type);
    if (document_item != nullptr) {
        document_item->setText(0, this->document->name.c_str());
        std::string document_attribute_text = "";
        for (auto attribute: document->attributes) {
            document_attribute_text += attribute.first + "=\"" + attribute.second + "\" ";
        }
        document_item->setText(1, document_attribute_text.c_str());
        document_item->setExpanded(true);
        this->nodeToItem(this->document, document_item);
    }
}


void QTreeWidgetDOMRenderer::setTreeWidget(QTreeWidget* tree_widget)
{
    this->tree_widget = tree_widget;
}


QTreeWidget* QTreeWidgetDOMRenderer::getTreeWidget()
{
    return this->tree_widget;
}
