/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "GUIHandler.h"


GUIHandler::GUIHandler(QObject *parent)
    : QObject(parent)
{
}


void GUIHandler::EmitNewTabRequest(QString url)
{
    emit NewTabRequest(url);
}


void GUIHandler::EmitNewTabRequestFromQAction()
{
    QUrl url = ((QAction*)QObject::sender())->data().toUrl();

    emit NewTabRequest(url.toString());
}


void GUIHandler::EmitURLChange(QUrl newURL)
{
    //TODO: check if a signal relay exists so that this won't be necessary

    emit URLChanged(QObject::sender(), newURL);
}


void GUIHandler::EmitTitleChange(QString newTitle)
{
    QObject* sender = QObject::sender();
    emit TitleChanged(sender, newTitle);
}


void GUIHandler::EmitURLHover(QString url, QString title, QString content)
{
    emit URLHovered(url);
}


void GUIHandler::EmitStatusBarText(QString newText)
{
    emit StatusBarTextChanged(newText);
}


void GUIHandler::EmitLoadStart()
{
    emit LoadStatusChanged(QObject::sender(), GUIHandler::LOAD_STATUS_STARTED);
}


void GUIHandler::EmitLoadFinished(bool success)
{
    if(success) {
        emit LoadStatusChanged(QObject::sender(), GUIHandler::LOAD_STATUS_FINISHED);
    } else {
        emit LoadStatusChanged(QObject::sender(), GUIHandler::LOAD_STATUS_ERROR);
    }
}
