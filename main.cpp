/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QApplication>


#include "MoeNavigatorMainWindow.h"
#include "DefaultSettings/DefaultPage.h"


int main(int argc, char *argv[])
{
    QApplication moenavigator(argc, argv);
    MoeNavigatorMainWindow w;

    //argument handling:
    if (moenavigator.arguments().length() == 2) {
        //two arguments: first is program name, second is URL
        QString url = moenavigator.arguments().at(1);
        w.openUrl(url);
    } else {
        w.loadMarkup(defaultPage);
    }

    w.show();
    return moenavigator.exec();
}
