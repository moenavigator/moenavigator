/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2021  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef BROWSERTABWIDGET_H
#define BROWSERTABWIDGET_H


#include <string>
#include <memory>


#include <QWidget>
#include <QTabWidget>
#include "GUIHandler.h"

#include "./EngineInterfaces/EngineInterface.h"
#include "./EngineInterfaces/EngineInterface_MNE.h"
#include "./Renderers/QTreeWidgetDOMRenderer.h"



class BrowserTabWidget: public QWidget
{
    Q_OBJECT


    protected:


    std::shared_ptr<QTreeWidgetDOMRenderer> console_dom_renderer;


    QTreeWidget* console_dom_tree = nullptr;


    bool console_enabled = false;


    public:


    BrowserTabWidget(
        QTabWidget* parent,
        std::shared_ptr<GUIHandler> gui_handler,
        std::string engine_name = "mne",
        QTreeWidget* console_dom_tree = nullptr
        );


    void setupEngine();


    void setEngine(std::string engine_name);


    std::string getEngineName();


    std::shared_ptr<EngineInterface> getEngine();


    virtual ~BrowserTabWidget();


    std::string engine_name;


    std::shared_ptr<EngineInterface> engine;


    QString url_text = "";


    std::shared_ptr<GUIHandler> gui_handler;


    public slots:


    void updateTitle(QString new_title);


    void updateTitle(QObject* sender, QString new_title);


    void setConsoleEnabled(bool enabled = true);
};


#endif
