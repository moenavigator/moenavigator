/*
    This file is part of MoeNavigator
    Copyright (C) 2012-2019  Moritz Strohm <ncc1988@posteo.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "BrowserTabWidget.h"


BrowserTabWidget::BrowserTabWidget(
    QTabWidget* parent,
    std::shared_ptr<GUIHandler> gui_handler,
    std::string engine_name,
    QTreeWidget* console_dom_tree
    )
    : QWidget(parent),
      gui_handler(gui_handler),
      engine_name(engine_name),
      console_dom_tree(console_dom_tree)
{
    setAttribute(Qt::WA_StaticContents);

    this->setSizePolicy(
        QSizePolicy::Expanding,
        QSizePolicy::Expanding
        );
    this->setLayout(new QGridLayout());

    this->engine = nullptr;
    this->setupEngine();
}


void BrowserTabWidget::setupEngine()
{
    if (this->engine != nullptr) {
        this->engine = nullptr;
    }

    std::shared_ptr<EngineInterface_MNE> mne_ptr = nullptr;
    if (this->engine_name == "mne") {
        mne_ptr = std::make_shared<EngineInterface_MNE>(this);
        this->engine = mne_ptr;
    }
    //No else here, since no other browser engine is supported at the moment.

    if ((this->engine != nullptr) && (this->gui_handler != nullptr)) {
        this->engine->setGUIHandler(this->gui_handler.get());
        this->connect(
            this->gui_handler.get(),
            SIGNAL(TitleChanged(QObject*,QString)),
            this,
            SLOT(updateTitle(QObject*, QString))
            );
    }
}


void BrowserTabWidget::setEngine(std::string engine_name)
{
    this->engine_name = engine_name;
    this->setupEngine();
}


std::string BrowserTabWidget::getEngineName()
{
    return this->engine_name;
}


std::shared_ptr<EngineInterface> BrowserTabWidget::getEngine()
{
    return this->engine;
}


void BrowserTabWidget::updateTitle(QString new_title)
{
    QTabWidget* parent = (QTabWidget*)this->parent();
    if (parent != nullptr) {
        int index = parent->indexOf(this);
        parent->setTabText(index, new_title);
    }
}


void BrowserTabWidget::updateTitle(QObject* sender, QString new_title)
{
    this->updateTitle(new_title);
}


BrowserTabWidget::~BrowserTabWidget()
{
    this->engine = nullptr;
}


void BrowserTabWidget::setConsoleEnabled(bool enabled)
{
    if (this->engine_name == "mne") {
        auto mne_interface = std::dynamic_pointer_cast<EngineInterface_MNE>(this->engine);
        if (mne_interface == nullptr) {
            return;
        }
        auto mne = mne_interface->getEngine();
        if (mne == nullptr) {
            return;
        }
        if (enabled) {
            if (this->console_dom_renderer == nullptr) {
                this->console_dom_renderer = std::make_shared<QTreeWidgetDOMRenderer>();
            }
            this->console_dom_renderer->setTreeWidget(this->console_dom_tree);
            mne->addRenderer(this->console_dom_renderer);
        } else {
            if (this->console_dom_renderer != nullptr) {
                mne->removeRenderer(this->console_dom_renderer);
            }
        }
    }
}
