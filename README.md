# MoeNavigator README

MoeNavigator is an attempt to write a web browser that is highly configurable,
secure, anonymous and fast. It is not another GUI for Chromium or a browser
engine like WebKit or Gecko. Instead, it has its own browser engine called
MoeNavigatorEngine.


## Installation

### Prerequisites

#### Qt5

MoeNavigator relies on Qt5. Please make sure that the Qt5 libraries
and development headers are installed before compiling MoeNavigator.

#### MoeNavigatorEngine

You also need to build and install MoeNavigatorEngine whose source
code can be found here:

https://codeberg.org/moenavigator/moenavigatorengine

#### Option: Avoid installing MoeNavigatorEngine

If you want to just build MoeNavigator, the easiest way is to just
clone the repository of MoeNavigatorEngine into the main folder
of the MoeNavigator repository with the following command:

    git clone https://codeberg.org/moenavigator/moenavigatorengine.git ./MoeNavigatorEngine

The folder of the engine must be named "MoeNavigatorEngine" so that the headers
are available to the source files of MoeNavigator. When compiling, the engine
will then be statically linked into the moenavigator executable.

__In this scenario, you must also make sure that the development headers
of the GnuTLS library are installed since MoeNavigatorEngine requires those!__


### Compiling

To compile MoeNavigator, you need to invoke cmake, followed by make.
To avoid messing up the source directory with build files, you can
create a subdirectory named "build" and call cmake from there:

    cmake ..

After that, you can run make as usual:

    make -jX

(X represents the number of parallel make jobs you want to run)

After make has finished, you will have the moenavigator executable
in the build directory. Enjoy browsing!


## Documentation

The documentation can be found in the MoeNavigator wiki at codeberg.org:

https://codeberg.org/moenavigator/moenavigator/wiki/index

The content of the wiki is also available as git submodule in the main
repository. To get the documentation, use the following two git commands
in the repository:

    git submodule init
    git submodule update

After that, the wiki content is available in the subfolder named "doc".
